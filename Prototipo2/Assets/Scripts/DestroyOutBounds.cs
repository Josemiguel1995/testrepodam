﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOutBounds : MonoBehaviour
{
    public float TopBound = 30;
    public float LowerBound = -14;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //condicion: si este objeto sobrepasa topBound, entonces se destruye
        if (transform.position.z >= TopBound)
        {
            Destroy(gameObject);
        }
        if (transform.position.z <= LowerBound)
        {
            Destroy(gameObject);
        }
    }
}