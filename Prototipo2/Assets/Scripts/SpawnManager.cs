﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject[] animalPrefabs;
    private float spawnRangeX = 20;
    private float spawnPosZ = 27;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))

            //Posicion aleatoria en x del animal que vamos a spawnear.
        {
            float xPos = Random.Range(-spawnRangeX, spawnRangeX);
            Vector3 SpawnPos = new Vector3(xPos, 0, spawnPosZ);

            //Elejimos un indice aleatorio para el array.
            int animalIndex = Random.Range(0, animalPrefabs.Length);

            //Elejimos un animal aleatorio de la lista.
            GameObject randomAnimal = animalPrefabs[animalIndex];
            Instantiate(randomAnimal, SpawnPos, randomAnimal.transform.rotation);
        }
    }
}
